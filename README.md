# Timeseries-Box-Jenkins #

### Summary ###

* Ancient codes using Box-Jenkins ARMA Models in predicting currency price
* Version 1.0

### Dependencies ###

* Python 2.7
* Numpy
* Scipy
* Statsmodels
* sklearn