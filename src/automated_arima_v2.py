# keep division a floating point operation
from __future__ import division
import os  
import numpy as np  
import pandas as pd  
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages  
import statsmodels.api as sm  
# from statsmodels.tsa.stattools import adfuller, acf
import multiprocessing as mp
from scipy import stats
# from datetime import datetime
# import statsmodels
from statsmodels.stats import diagnostic
from pandas.tseries.offsets import BDay # for generating business days
from statsmodels.tsa import stattools # manual acf and pacf

'''
History:
[1] Add Forecast function
[2] Iterate forecast by stitching previous forecasts
[3] Isolate printing of correlation and test, printing of stationarity and test
[4] Automate extraction of p,q for selecting ARMA order
[5] Add main routine from extraction of data to reconstruction of forecasted values - current
'''
# References:
# http://www.johnwittenauer.net/a-simple-time-series-analysis-of-the-sp-500-index/
# http://statsmodels.sourceforge.net/devel/examples/notebooks/generated/tsa_arma.html
# https://www.otexts.org/fpp/8/1
# https://www.analyticsvidhya.com/blog/2016/02/time-series-forecasting-codes-python/
# http://conference.scipy.org/proceedings/scipy2015/pdfs/margaret_mahan.pdf
# http://ucanalytics.com/blogs/step-by-step-graphic-guide-to-forecasting-through-arima-modeling-in-r-manufacturing-case-study-example/
# TODO: arma vs Neural Network predictive power
# http://www.geocomputation.org/1998/05/gc_05.htmhttp://www.geocomputation.org/1998/05/gc_05.htm


LAG = 40
TRADINGDAYS = 252 # https://en.wikipedia.org/wiki/Trading_day
FILEPATH = '/home/akalingking/dataset/oanda/EURUSD1440.csv'
RESULTPATH = '/home/akalingking/src/python/v2/oanda_analytics/results/'
PLTFONTSIZE = 8
RCPARAMS = {'axes.titlesize':PLTFONTSIZE,
             'axes.labelsize':PLTFONTSIZE,
             'legend.fontsize':PLTFONTSIZE,
             'xtick.labelsize':PLTFONTSIZE,
             'ytick.labelsize':PLTFONTSIZE,}

def read_ts_from_csv(filepath, col='Close'):
    '''
    @brief constructs dataframe from csv file then returns selected series
    @param filepath
    @param col
    @rtype pandas.Series
    '''
    # static
    read_ts_from_csv.headers_ = ['Datetime','Open','High','Low','Close', 'Volume']
    ret = None
    raw_data = None
    
    print('readfile %s' % filepath)
    
    # oanda dataset uses first 2 columns for data and time separately
    parse_dates_ = [[0,1]]
    try:
        dateparser = lambda x,y: pd.datetime.strptime(x+" "+y, '%Y.%m.%d %H:%M')
        raw_data = pd.read_csv(FILEPATH, 
                     delimiter=',', 
                     header=None, 
                     parse_dates= parse_dates_,
                     date_parser=dateparser)
        
        raw_data.columns = read_ts_from_csv.headers_
        raw_data.set_index(['Datetime'], drop=True, inplace=True)
        print raw_data.info()
        print raw_data.head(5)
    except:
        print ("Exception reading source file")
    
    if raw_data is not None:
        try:
            ret = pd.Series(data=raw_data[col].values, index=raw_data.index)
        except Exception as e:
            print ('Exception: %s' % e)   
    return ret

def plot_ts(data, title=None):
    '''
    @param data datafram instance
    '''
    fig, axes = plt.subplots(nrows=1, ncols=1, sharex=False, sharey=False, figsize=(12,10))
    plt.rcParams.update(RCPARAMS)
    data.plot.line(ax=axes)
    
    fig.autofmt_xdate()
    # set font size
    plt.setp([a.get_xticklabels() for a in fig.axes], visible=True, fontsize=5)
    plt.setp([a.get_yticklabels() for a in fig.axes], visible=True, fontsize=5)
     
    # Choose layout spacing
    plt.tight_layout(pad=2, w_pad=0.0, h_pad=1.0)
#         plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=None, hspace=None)
    # Set top title
    plt.suptitle(title, y=1.0)
    # save to pdf
    from matplotlib.backends.backend_pdf import PdfPages
    pp = PdfPages('./results/' + title + '.pdf')
    plt.savefig(pp, format='pdf')
    pp.close()
#     show to desktop, call is blocking
#     plt.show()


def plot_stationarity(data, title, window):
    '''
    @param data    series
    @param title   name of series
    @param window  lag  
    '''
    '''
    [1] Variables for stationarity plots
    '''
    rollingMean = pd.Series.rolling(data, window=window).mean()
    rollingStd = pd.Series.rolling(data, window=window).std()
    s = pd.DataFrame(data=data, index=data.index, columns=['Original'])
    s['Rolling-Mean'] = rollingMean
    s['Rolling-Stdev'] = rollingStd
    '''
    [2] Decompose timeseries
    '''
    composition = sm.tsa.seasonal_decompose(data, model='additive', freq=window)
    '''
    [3] Show plots
    '''    
    fig, axes = plt.subplots(nrows=4, ncols=1, sharex=False, sharey=False, figsize=(10,15))
    plt.rcParams.update(RCPARAMS)
    
    #{@ Plot
    s.plot(ax=axes[0], title='Data', legend='best')
    composition.trend.plot(ax=axes[1], title='Trend')
    composition.seasonal.plot(ax=axes[2], title='Seasonal')
    composition.resid.plot(ax=axes[3], title='Residual')
    #}@plot

    fig.autofmt_xdate()
    # set font size
    plt.setp([a.get_xticklabels() for a in fig.axes], visible=True, fontsize=PLTFONTSIZE)
    plt.setp([a.get_yticklabels() for a in fig.axes], visible=True, fontsize=PLTFONTSIZE)
    
    # Choose layout spacing
    plt.tight_layout(pad=3, w_pad=0.0, h_pad=2.0)
#         plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=None, hspace=None)
    # Set top title
    plt.suptitle(title, y=1.0)
    
    # save to pdf
    pp = PdfPages(RESULTPATH +'stationarity_' + title + '.pdf')
    plt.savefig(pp, format='pdf')
    pp.close()
    #show to desktop, call is blocking
#     plt.show()


def plot_autocorrelation(data, title, window=40, ymax=0.05):
#     from statsmodels.graphics import gofplots
    '''
    Show plots
    '''
    # Prepare figure    
    fig, axes = plt.subplots(nrows=4, ncols=1, sharex=False, sharey=False, figsize=(10,15))
    plt.rcParams.update(RCPARAMS)
    # plot the original data
    data.plot.line(ax=axes[0], title=title)
    
    #{ plot
    acf_ = stattools.acf(data, nlags=window) # actual data, just to get range of diplay
    sm.graphics.tsa.plot_acf(data.values.squeeze(), lags=window, ax=axes[1])
    ymax = np.sort(acf_)[-2] *1.5
    axes[1].set_ylim(-ymax, ymax)
    
    pacf_ = stattools.pacf(data, nlags=window) # actual data, just to get range of diplay
    sm.graphics.tsa.plot_pacf(data, lags=window, ax=axes[2])
    ymax = np.sort(pacf_)[-2] *1.5 
    axes[2].set_ylim(-ymax, ymax)
    
    # http://statsmodels.sourceforge.net/devel/generated/statsmodels.graphics.gofplots.qqplot.html
    sm.qqplot(data=data, ax=axes[3], line='r')
    #} plot
    
    fig.autofmt_xdate()
    # set font size
    plt.setp([a.get_xticklabels() for a in fig.axes], visible=True, fontsize=PLTFONTSIZE)
    plt.setp([a.get_yticklabels() for a in fig.axes], visible=True, fontsize=PLTFONTSIZE)
    # Choose layout spacing
    plt.tight_layout(pad=3, w_pad=0.0, h_pad=2.0)
#         plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=None, hspace=None)
    # Set top title
    plt.suptitle('Correlation: ' + title, y=1.0)
    
    # save to pdf
    pp = PdfPages(RESULTPATH + 'correlation_' + title + '.pdf')
    plt.savefig(pp, format='pdf')
    pp.close()
    #show to desktop, call is blocking
#     plt.show()


def is_stationary(ts, name, window=40, crit=0.05):
    assert (crit is '1%' or crit is '5%' or crit is '10%')
    # http://stackoverflow.com/questions/279561/what-is-the-python-equivalent-of-static-variables-inside-a-function
    is_stationary.crits_ = {0.01:'1%', 0.05:'5%', 0.1:'10%'}
    ret = False
    '''
    [1] Unit-Root Test
    '''
    # Augmented Dickey-Fuller Test
    # Test statistic must be smaller thatn the crititical value
    # H_0 == Has Unit-Root (not stationary)
    # H_1 == Has no Unit-Root (stationary)
    # if test-statistic is smalller, we reject the null hypthesis
    # Alternatively, if p-value is less than 0.05 reject the null hypothesis
    # http://statsmodels.sourceforge.net/devel/generated/statsmodels.tsa.stattools.adfuller.html
    dftest = stattools.adfuller(ts.values, autolag='AIC')
    #@{print result to console
    critical_values = 'critical values:\n'
    for key,value in dftest[4].items():
        critical_values += '(%s)\t\t%0.8f\n' % (key,value)
    print ('\nADF Test:\t%s\n'
           't-statistic\t%.8f\n'
           'p-value\t\t%.8f\n'
           'lags used\t%d\n'
           'noobs\t\t%d\n'
           '%s\n' % (name, dftest[0], dftest[1], dftest[2], dftest[3], critical_values))
    #@}
    pvalue = dftest[1]
    tstatistic = dftest[0]
    critical_value = dftest[4][is_stationary.crits_[crit]]
    if (tstatistic > critical_value):
        if (pvalue > crit):
            ret = True
    return ret
            
def is_autocorrelated(x, name, window=40):
    lb_pass = False
    dw_pass = False
    '''
    [1] LJung-Box Test
    ''' 
    # https://www.r-bloggers.com/story-of-the-ljung-box-blues-progress-not-perfection/
    # http://www.itl.nist.gov/div898/handbook/pmc/section4/pmc4481.htm
    # Ljung-box requires input to be stationary and has a mean of 0.
    # Test normality
    # H0: The data are independently distributed (i.e. the correlations in the population 
    #     from which the sample is taken are 0, so that any observed correlations in the 
    #     data result from randomness of the sampling process).
    # Ha: The data are not independently distributed; they exhibit serial correlation.   
    lb = diagnostic.acorr_ljungbox(x, lags=window, boxpierce=False)
    # p-values must be greater than critical values eg 0.05
    pvals = lb[1]
    rejection_region = pvals[pvals < 0.05]
    if len(rejection_region) <= 0:
        lb_pass = True
        print ("%s is independently distributed, not autocorrelated" % (name))
    else:
        print ("%s is NOT independently distributed" % (name))
        
    '''
    [2] Durbin-Watson
    The Durbin Watson statistic is a number that tests for autocorrelation in the residuals 
    from a statistical regression analysis. The Durbin-Watson statistic is always between 0 and 4.
    A value of 2 means that there is no autocorrelation in the sample. Values approaching 0 
    indicate positive autocorrelation and values toward 4 indicate negative autocorrelation.
    Note: Durbin-watson result is compromised if the model uses AR component
    http://stats.stackexchange.com/questions/154167/why-ever-use-durbin-watson-instead-of-testing-autocorrelation 
    '''
    durbin_watson = sm.stats.durbin_watson(x)
    durbin_watson_rounded = np.math.ceil(durbin_watson*(10**1)/(10**1))
    print ("%s durbin-watson test is %0.8f (rounded)=%0.8f" % (name, durbin_watson, durbin_watson_rounded))
    
    if (durbin_watson_rounded == 2.0):
        dw_pass = True
    else:
        print ("%s failed Durbin-Watson test" % (name))
    
    return lb_pass and dw_pass


def get_pq(x, lags=40, alpha=0.05):
    '''
    https://people.duke.edu/~rnau/411arim3.htm
    Detrermines the ARMA parameters where p is determined from pacf and used
    as parameter for AR component and q from the acf test for the MA component
    Note:
    [1] If you have correctly identified the model,the highest-order AR or MA coefficient should 
    be significantly different from zero according to the usual standard for a regression model, 
    i.e., it should have a t-stat greater than 2 in magnitude and correspondingly a P-value less 
    than 0.05. In applying this test, you only look at the highest order coefficient, not the lower-
    order ones.
    
    [2] ACF that dies out gradually and PACF that cuts off  sharply after a few lags, 
        usually positively correlated at lag 1 (or non-stationary), AR signature
    [3] ACF that cuts off sharply after a few lags and PACF that dies out more gradually 
        usually negatively correlated at lag 1 (or over-differenced), AR signature
    [4] If ACF is gradually decreasing, add differencing
    '''
    assert (len(x) >= lags)
    # internal static function
    def find_significant_lags(lags, qstat, pvals):
        lags_ = [] # significant lags
        for i in xrange(0, len(lags)):
            lag = p[i]
            print ("get_pq: [%d] qstat=%0.8f pval=%0.8f" % (lag, qstat[lag], pvals[lag]))
            if (qstat[lag] >= 2.0 and pvals[lag] > alpha):
                lags_.append(lags[i])
        return lags_
    
    def find_lags(x, interval):
        lags = []
        for i in xrange(1, len(x)-1):
            val = x[i]
            if val > 0 and val > interval:
                lags.append(i)
            if (val < 0 and val < -interval):
                lags.append(i)
        return lags
                
    #z=1.96
    z = stats.norm.ppf(1-alpha)
    print 'get_pq:z=', z
    interval = z / np.sqrt(len(x))
    print 'get_pq:interval=', interval
    
    
    acf, confInt, qstat, pvals  = stattools.acf(x, nlags=lags, alpha=alpha, qstat=True)
    p = find_lags(acf, interval)
    p_ = find_significant_lags(p, qstat, pvals)
    if (len(p_) <= 0):
        print ("get_pq: No significant ACF lags")
        
    pacf, _  = stattools.pacf(x, nlags=lags, alpha=alpha)
    q = find_lags(pacf, interval)
    
    return p, q

def predict_arma(x, title, order):
    '''
    [1] Train
    '''
    assert (x.shape[0])
    model = sm.tsa.ARMA(x, order=order)
    result = model.fit(disp=-1)
    y = result.fittedvalues
    resid = result.resid
        
    '''
    [2] Validate model
    '''
    '''
    [3] Test residual for serial correlation
    ''' 
    is_autocorrelated(resid, ('%s_arma_%s_residual' % (title,order)), 40)
    # the lower aic and bic the better
    rss = np.sum((y - x)**2)
    rmse = sm.tools.eval_measures.rmse(x.values, y.values)
    print('\nARMA results for %s\naic:\t\t%0.8f\nbic:\t\t%0.8f\nrss:\t\t%0.8f\nrmse:\t\t%0.8f\n' % 
          (title, result.aic, result.bic, rss, rmse))
    
    #{@print results
    df = pd.DataFrame(data={'x':x.values, 'y':y.values}, index=x.index)
    mp.Process(target=plot_ts, args=(df, 'arma_' + title)).start()
    #print residual
    resid_ = pd.DataFrame(data=resid)
    mp.Process(target=plot_ts, args=(resid_, "arma_residual_"+title)).start()
    mp.Process(target=plot_stationarity, args=(resid, 'arma_resid_'+title, TRADINGDAYS)).start()
    mp.Process(target=plot_autocorrelation, args=(resid_, 'arma_resid_'+title)).start()
    #}@print results
    return result
    
    
def forecast_arma(x, result, steps=1, useiteration=True):
    '''
    @param x
    @type x: pandas.Series
    @param result
    @type result: ARMAResult
    @param steps
    @return Series
    https://groups.google.com/forum/#!topic/pystatsmodels/_ItLBVpePIY
    http://machinelearningmastery.com/time-series-forecast-uncertainty-using-confidence-intervals-python/
    '''
    if (useiteration):
        order = (result.k_ar, result.k_ma)
        print "order ", order
    
        for i in xrange(0, steps):
            pred, sterr, confint= result.forecast(steps=1)
            
            next_date = (x.index[-1] + BDay(1)).to_pydatetime()
            x = x.append(pd.Series(pred[0], index=[next_date]))
    
            model = sm.tsa.ARMA(x[i:], order=order)
            result = model.fit(disp = -1)
            y = result.fittedvalues
            rmse = sm.tools.eval_measures.rmse(x.values[i:], y.values)
            print ("iteration %d rmse=%0.8f" % (i, rmse))
            # Check residual
            resid = result.resid 
            lb = diagnostic.acorr_ljungbox(resid, lags=40, boxpierce=False)
            # p-values must be greater than critical values eg 0.05
            pvals = lb[1]
            rejection_region = pvals[pvals < 0.05]
            if (len(rejection_region) > 0):
                print ("iteration %d failed ljung-box test")
                break
        
        return x.tail(steps)
    else:
        pred, sterr, confint= result.forecast(steps=steps)
        forecast = pd.Series()
        # stich pred to date steps
        current_date = x.index[-1]
        for i in xrange(0, steps):
            next_date = (current_date + BDay(1)).to_pydatetime()
            forecast.append(pd.Series(pred[0], index=[next_date]))
            current_date = next_date
        return forecast
    
def diffinv(diff, initial):
    ret = pd.Series(0.0, index=diff.index)
    prev = initial
    for i in xrange(0, len(diff)):
        ret[i] = prev = prev + diff[i]
    return ret
    

def main():
    '''
    [1] Constructs ts from csv file
    '''
    close = read_ts_from_csv(FILEPATH, col='Close')
    assert (close is not None)
    print close.tail(5)
    
    '''
    2. Start ts processing
    '''
    '''
    Pre-process: test stationarity
    https://www.otexts.org/fpp/8/1
    '''
#     close = pd.Series(data=raw_data['Close'].values, index=raw_data.index) 
    mp.Process(target=plot_stationarity, args=(close, 'Close', TRADINGDAYS)).start()
    mp.Process(target=plot_autocorrelation, args=(close, 'Close', 40)).start()
    
    '''
    Remove Trend: Differencing
    '''
#     data = pd.DataFrame(raw_data['Close'], index=raw_data.index, columns=['Close'])
    data = pd.DataFrame(data=close.values, index=close.index, columns=['Close'])
    data['Log'] = np.log(data['Close'])
    data['Diff'] = data['Close'] - data['Close'].shift(1)
    data['LogDiff'] = data['Log'] - data['Log'].shift(1)
    
    data['Diff2'] = data['Close'] - data['Close'].shift(2)

    # Create Timeseries variables
#     close = pd.Series(data=data['Close'].values, index=data.index)
    
    # Remove trend
    log = pd.Series(data=data['Log'].values, index=data.index)
    log.dropna(inplace=True)
    
    # Remove seaonality
    diff = pd.Series(data=data['Diff'].values, index=data.index)
    diff.dropna(inplace=True)
    
    logdiff = pd.Series(data=data['LogDiff'].values, index=data.index)
    logdiff.dropna(inplace=True)
    
    diff2 = pd.Series(data['Diff2'].values, index=data.index)
    diff2.dropna(inplace=True)

    '''
    Test Stationarity
    '''
    mp.Process(target=plot_stationarity, args=(diff2, 'Diff2', TRADINGDAYS)).start()
    
    '''
    Test Autocorrelation
    ''' 
#     mp.Process(target=test_autocorrelation, args=(close, 'Close', 40)).start()
#     mp.Process(target=test_autocorrelation, args=(diff, 'Diff', 40)).start()
#     mp.Process(target=test_autocorrelation, args=(log, 'Log', 40)).start()
#     mp.Process(target=test_autocorrelation, args=(logdiff, 'LogDiff', 40)).start()
    mp.Process(target=plot_autocorrelation, args=(diff2, 'Diff2', 40)).start()
      

    '''
    Perform ARIMA forecast
    '''
#     mp.Process(target=forecast_arma, args=(log, 'Log')).start()
#     mp.Process(target=forecast_arma, args=(diff, 'Diff')).start()
#     mp.Process(target=forecast_arma, args=(diff2, 'Diff2', (2,1))).start()
#     mp.Process(target=forecast_arima, args=(logdiff, 'Diff', (1,1,0))).start()

#     p,q = get_pq(diff2)
#     print p
#     print q
#     return
    result = predict_arma(diff2, 'Diff2', (1,1))
    if result is not None:
        print result.summary()
        diff2_forecast = forecast_arma(diff2, result, steps=5)
        print "Forecasted result:\n", diff2_forecast
        # recover actual values
        diff_forecast = diffinv(diff2_forecast, diff[-1])
        close_forecast = diffinv(diff_forecast, close[-1])
        print close_forecast
    
        
    else:
        print "Failed to generate ARMA Model"
    
if __name__=='__main__':
    main()
