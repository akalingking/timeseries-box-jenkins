import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
import statsmodels.api as sm
import matplotlib
import datetime
from pandas.tools.plotting import autocorrelation_plot
from scipy.stats import norm

DATAPATH = '/home/akalingking/dataset/oanda/'

def main():
    # Visualization
    # http://stackoverflow.com/questions/4150171/how-to-create-a-density-plot-in-matplotlib
    # http://pandas.pydata.org/pandas-docs/version/0.13.1/visualization.html
    # http://conference.scipy.org/proceedings/scipy2011/pdfs/statsmodels.pdf
    # Sample Autocorrelation plots http://pandasplotting.blogspot.com/2012/06/autocorrelation-plot.html
    # 
    # Statistics 
    # https://onlinecourses.science.psu.edu/stat501/node/357
    # http://www.itl.nist.gov/div898/handbook/pmc/section4/pmc4463.htm
    # 
    # Forecasting
    # SPX example http://www.johnwittenauer.net/a-simple-time-series-analysis-of-the-sp-500-index/
    # https://www.analyticsvidhya.com/blog/2016/02/time-series-forecasting-codes-python/
    # https://bicorner.com/2015/11/16/time-series-analysis-using-ipython/
    #
    
    '''
    1. Construct data
    '''
    headers = ['Datetime','Open','High','Low','Close', 'Volume']
    parse_dates = [[0,1]]
    # parse from the original mql date format
    dateparser = lambda x,y: pd.datetime.strptime(x+" "+y, '%Y.%m.%d %H:%M')
    data = pd.read_csv(DATAPATH+'EURUSD1440.csv', 
                     delimiter=',', 
                     header=None, 
                     parse_dates=parse_dates,
                     date_parser=dateparser)
    
    data.columns = headers
    data.set_index(['Datetime'],drop=True, inplace=True)
    print data.info()
    
    # Obtain target ts
    close = data['Close']
    print close.describe()
    print close.head(10)
    
    # DEscriptive Stats
    mean = close.mean()
    std = close.std()
    median = close.median()
    mode = close.mode()
    kurt = close.kurt()
    skew = close.skew()
    
    returns = close.diff()/close.shift()
    returns[0] = 0 # set NaN to numeric
#     print returns.head(10)
    
    '''
    2. Start plotting data visualization
    '''
#     fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(10,15))
    fig, axes = plt.subplots(nrows=4, ncols=2, sharex=False, sharey=False)
   
#     data.plot.line(y=['Close'], ax=axes[0,0], title="Close", legend=None)
    close.plot.line(y=close[1], x=close.index.values, ax=axes[0,0], title="Close", legend=None)
#     close.plot.line(y=close[1], x=close.index.values, title="Close", legend=None)
#     axes[0].get_xaxis().set_major_formatter(matplotlib.dates.DateFormatter('%Y%m%d'))
#     plt.sca(axes[0, 0])
#     plt.xticks(range(100), data.index.values, color='red')
#     axes[0,0].set_xlabel("Date")
#     plt.setp(axes[0,0].get_xticklabels(), visible=True)
    
    returns.plot.line(ax=axes[0,1], title='Returns', legend=None)
    
    close.plot.kde(ax=axes[1,0], title='Kernel Density')
    axes[1,0].text(1.95,0.7,
                   "mu = %0.4f\nstd = %0.4f\nmed = %0.4f\nmod = %0.4f\nkrt = %0.4f\nskw = %0.4f\n" % (mean, std, median, mode, kurt, skew),
                   fontsize=8)
    
    close.plot.hist(bins=100, ax=axes[1,1], title='Histogram')
 
#     # Dickey-Fuller test test for stationarity
    sm.graphics.tsa.plot_acf(close.squeeze(), lags=200, ax=axes[2,0])
     
    sm.graphics.tsa.plot_pacf(close.squeeze(), lags=200, ax=axes[2,1])
    
    '''
    Autocorrelation plots are often used for checking randomness in time series. 
    This is done by computing autocorrelations for data values at varying time lags. 
    If time series is random, such autocorrelations should be near zero for any 
    and all time-lag separations. If time series is non-random then one or more of 
    the autocorrelations will be significantly non-zero. The horizontal lines 
    displayed in the plot correspond to 95% and 99% confidence bands. The dashed 
    line is 99% confidence band.
    '''
    autocorrelation_plot(close, ax=axes[3,0])
    '''
    End of plots
    '''
    
    '''
    3. Display plots
    '''
    # format date axis
    fig.autofmt_xdate()
    # set font size
    plt.setp([a.get_xticklabels() for a in fig.axes], visible=True, fontsize=10)
    plt.setp([a.get_yticklabels() for a in fig.axes], visible=True, fontsize=10)
    # Choose layout spacing
    plt.tight_layout(pad=0.1, w_pad=0.1, h_pad=0.1)
    # Set top title
    plt.suptitle('EURDLR Daily', y=1.0)
    plt.show()
    

if __name__=='__main__':
    main()