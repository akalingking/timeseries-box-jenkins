import os  
import numpy as np  
import pandas as pd  
import matplotlib.pyplot as plt  
import statsmodels.api as sm  
import seaborn as sb  

# References:
# http://www.johnwittenauer.net/a-simple-time-series-analysis-of-the-sp-500-index/
# http://statsmodels.sourceforge.net/devel/examples/notebooks/generated/tsa_arma.html
# https://www.otexts.org/fpp/8/1
# https://www.analyticsvidhya.com/blog/2016/02/time-series-forecasting-codes-python/

# sb.set_style('darkgrid')
# path = os.getcwd() + '\data\stock_data.csv'  
# stock_data = pd.read_csv(path)  
# stock_data['Date'] = stock_data['Date'].convert_objects(convert_dates='coerce')  
# stock_data = stock_data.sort_index(by='Date')  
# stock_data = stock_data.set_index('Date')  

FILEPATH = '/home/akalingking/dataset/oanda/EURUSD1440.csv'

def main():
    '''
    Constructs dataframe ts from csv file
    '''
    print('readfile %s' % FILEPATH)
    data = None
    headers_ = ['Datetime','Open','High','Low','Close', 'Volume']
    # oanda dataset uses first 2 columns for data and time separately
    parse_dates_ = [[0,1]]
    try:
        dateparser = lambda x,y: pd.datetime.strptime(x+" "+y, '%Y.%m.%d %H:%M')
        data = pd.read_csv(FILEPATH, 
                     delimiter=',', 
                     header=None, 
                     parse_dates= parse_dates_,
                     date_parser=dateparser)
        
        data.columns = headers_
        data.set_index(['Datetime'], drop=True, inplace=True)
    except:
        print ("Exception reading source file")

    print data.info()
    print data.head(10)  
    
    # Prepare figure
    fig, axes = plt.subplots(nrows=10, ncols=1, sharex=True, sharey=False, figsize=(10,15))
    data['Close'].plot.line(ax=axes[0],title='Close')
    
    
    
    
    '''
    2. Start ts processing
    ''' 
    data['FirstDiff'] = data['Close'] - data['Close'].shift(1)
    data['FirstDiff'].plot.line(ax=axes[1], title='Diff_1')
    
    # Perform log transform
    data['Natural_Log'] = data['Close'].apply(lambda x: np.log(x)) 
    data['Natural_Log'].plot.line(ax=axes[2], title='Natural_Log')
    
    
    # Check Variance
    data['Original_Variance'] = pd.Series.rolling(data['Close'], window=30, min_periods=None, freq=None, center=True).var()  
    data['Log_Variance'] = pd.Series.rolling(data['Natural_Log'], window=30, min_periods=None, freq=None, center=True).var()
    data['Original_Variance'].plot.line(ax=axes[3], title='Original_Variance')
    data['Log_Variance'].plot.line(ax=axes[4], title='Log_Variance')
    
    data['Log_Dif_1'] = data['Natural_Log'] - data['Natural_Log'].shift(1)
    data['Log_Dif_1'].plot.line(ax=axes[5], title='Log_Dif_1')
    
    
    # Create Lag variables
#     data['Lag_1'] = data['Log_Dif_1'].shift(1)
    # Make a scatter plot
#     sb.jointplot(x=data['Log_Dif_1'], y=data['Lag_1'], kind='reg', size=13)
    # We need to see each lag characteristic compared to the original series
    # A shortcut to see better is to use acf and pacf
    
    decomposition = sm.tsa.seasonal_decompose(data['Natural_Log'], model='additive', freq=30)  
#     fig1 = plt.figure()  
#     decomposition.plot() 
    # hmm not seen in pydev
    decomposition.trend.plot(ax=axes[6], title='Trend')
    decomposition.seasonal.plot(ax=axes[7], title='Seasonal')
    decomposition.resid.plot(ax=axes[8], title='Residual')
    
    '''
    Perform ARIMA forecast
    '''
    model = sm.tsa.ARIMA(data['Natural_Log'].iloc[1:], order=(1, 0, 0))  
    results = model.fit(disp=-1)  
    data['Forecast'] = results.fittedvalues  
    data[['Natural_Log', 'Forecast']].plot.line(ax=axes[9], title='ARIMA', figsize=(16, 12))  
    
    '''
    3. Display plots
    '''
    # format date axis
    fig.autofmt_xdate()
    # set font size
    plt.setp([a.get_xticklabels() for a in fig.axes], visible=True, fontsize=8)
#     plt.setp([a.get_yticklabels() for a in fig.axes], visible=True, fontsize=8)
    
    # Choose layout spacing
    plt.tight_layout(pad=2, w_pad=0.0, h_pad=1.0)
#         plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=None, hspace=None)
    # Set top title
    plt.suptitle("Forecast: ARIMA", y=1.0)
    # save to pdf
    from matplotlib.backends.backend_pdf import PdfPages
    pp = PdfPages('./results/forecast_arima.pdf')
    plt.savefig(pp, format='pdf')
    pp.close()
    #show to desktop, call is blocking
    plt.show()
    
    
if __name__=='__main__':
    main()