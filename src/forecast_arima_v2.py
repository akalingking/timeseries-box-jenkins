import os  
import numpy as np  
import pandas as pd  
import matplotlib.pyplot as plt  
import statsmodels.api as sm  
from statsmodels.tsa.stattools import adfuller
from pandas.tools.plotting import autocorrelation_plot
import seaborn as sb  
import multiprocessing as mp
from scipy import stats


# References:
# http://www.johnwittenauer.net/a-simple-time-series-analysis-of-the-sp-500-index/
# http://statsmodels.sourceforge.net/devel/examples/notebooks/generated/tsa_arma.html
# https://www.otexts.org/fpp/8/1
# https://www.analyticsvidhya.com/blog/2016/02/time-series-forecasting-codes-python/

# sb.set_style('darkgrid')
# path = os.getcwd() + '\data\stock_data.csv'  
# stock_data = pd.read_csv(path)  
# stock_data['Date'] = stock_data['Date'].convert_objects(convert_dates='coerce')  
# stock_data = stock_data.sort_index(by='Date')  
# stock_data = stock_data.set_index('Date')  

FILEPATH = '/home/akalingking/dataset/oanda/EURUSD1440.csv'
PLTFONTSIZE = 8
RCPARAMS = {'axes.titlesize':PLTFONTSIZE,
             'axes.labelsize':PLTFONTSIZE,
             'legend.fontsize':PLTFONTSIZE,
             'xtick.labelsize':PLTFONTSIZE,
             'ytick.labelsize':PLTFONTSIZE,}

def test_stationarity(data, title, window):
    rollingMean = pd.Series.rolling(data, window=window).mean()
    rollingStd = pd.Series.rolling(data, window=window).std()
    
    print '\nDickey-Fuller Test:'
    dftest = adfuller(data, autolag='AIC')
    dfoutput = pd.Series(dftest[0:4], index=['Test Statistic','p-value','#Lags Used','Number of Observations Used'])
    for key,value in dftest[4].items():
        dfoutput['Critical Value (%s)'%key] = value
    print dfoutput
    
    s = pd.DataFrame(data=data, index=data.index, columns=['Original'])
    s['Mean'] = rollingMean
    s['Stdev'] = rollingStd
    fig, axes = plt.subplots(nrows=6, ncols=1, sharex=False, sharey=False, figsize=(10,15))
    plt.rcParams.update(RCPARAMS)
    s.plot(ax=axes[0], title='Data', legend='best')
    
    rollingMean.plot.line(ax=axes[1], color='blue', title="Rolling Mean");
    rollingStd.plot.line(ax=axes[2], color='red', title='Rolling Std');
    
    '''
    Decompose
    '''
    decomposition = sm.tsa.seasonal_decompose(data, model='additive', freq=window)  
    decomposition.trend.plot(ax=axes[3], title='Trend')
    decomposition.seasonal.plot(ax=axes[4], title='Seasonal')
    decomposition.resid.plot(ax=axes[5], title='Residual')
    
    '''
    Show plots
    '''
    fig.autofmt_xdate()
    # set font size
    plt.setp([a.get_xticklabels() for a in fig.axes], visible=True, fontsize=PLTFONTSIZE)
    plt.setp([a.get_yticklabels() for a in fig.axes], visible=True, fontsize=PLTFONTSIZE)
    
    # Choose layout spacing
    plt.tight_layout(pad=3, w_pad=0.0, h_pad=2.0)
#         plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=None, hspace=None)
    # Set top title
    plt.suptitle(title, y=1.0)
    # save to pdf
    from matplotlib.backends.backend_pdf import PdfPages
    pp = PdfPages('./results/' + title + '_descriptive.pdf')
    plt.savefig(pp, format='pdf')
    pp.close()
    #show to desktop, call is blocking
#     plt.show()

def test_autocorrelation(data, title, window):
    # Prepare figure    
    fig, axes = plt.subplots(nrows=5, ncols=1, sharex=False, sharey=False, figsize=(10,15))
    plt.rcParams.update(RCPARAMS)
    
    data.plot.line(ax=axes[0], title=title)

    sm.graphics.tsa.plot_acf(data, lags=window, ax=axes[1])
    axes[1].set_ylim(np.min(data), np.max(data))
     
    sm.graphics.tsa.plot_pacf(data, lags=window, ax=axes[2])
    axes[2].set_ylim(np.min(data), np.max(data))
     
#     autocorrelation_plot(data, ax=axes[3])
#     axes[3].set_ylim(np.min(data), np.max(data))

    
    #altrenative, manual?
    # http://statsmodels.sourceforge.net/0.6.0/generated/statsmodels.tsa.stattools.pacf.html
    from statsmodels.tsa import stattools
    z = 1.96#stats.norm.ppf(0.95)
    ax = axes[3]
    pacf = stattools.pacf(data, nlags=window, method='ols')
    pacf_ = pd.DataFrame(data=pacf, columns=['PACF'])
    pacf_.plot.line(ax=ax, title='PACF', label=None)
    
    axline = z / np.sqrt(pacf_.shape[0])
    c = '#660099'
    ax.set_ylim(-axline * 1.1, axline * 1.1)
    ax.axhline(y=axline, linestyle='--', color=c)
    ax.axhline(y=-axline, linestyle='--', color=c)
    
    ax = axes[4]
    acf = stattools.acf(data, nlags=window)
    acf_ = pd.DataFrame(data=acf, columns=['ACF'])
    acf_.plot.line(ax=ax, title='ACF', label=None)
    axline = z / np.sqrt(acf_.shape[0])
    c = '#660099'
    ax.set_ylim(-axline * 1.1, axline * 1.1)
    ax.axhline(y=axline, linestyle='--', color=c)
    ax.axhline(y=-axline, linestyle='--', color=c)
    
    '''
    Show plots
    '''
    fig.autofmt_xdate()
    # set font size
    plt.setp([a.get_xticklabels() for a in fig.axes], visible=True, fontsize=PLTFONTSIZE)
    plt.setp([a.get_yticklabels() for a in fig.axes], visible=True, fontsize=PLTFONTSIZE)
    
    # Choose layout spacing
    plt.tight_layout(pad=3, w_pad=0.0, h_pad=2.0)
#         plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=None, hspace=None)
    # Set top title
    plt.suptitle(title, y=1.0)
    # save to pdf
    from matplotlib.backends.backend_pdf import PdfPages
    pp = PdfPages('./results/' + title + '_correlation.pdf')
    plt.savefig(pp, format='pdf')
    pp.close()
    #show to desktop, call is blocking
#     plt.show()

def print_forecast(data, title):
    assert (data.shape[1] == 2)
    
    fig, axes = plt.subplots(nrows=1, ncols=1, sharex=False, sharey=False, figsize=(10,15))
    plt.rcParams.update(RCPARAMS)
    data.plot.line(ax=axes, figsize=(16, 12))
    
    fig.autofmt_xdate()
    # set font size
    plt.setp([a.get_xticklabels() for a in fig.axes], visible=True, fontsize=5)
    plt.setp([a.get_yticklabels() for a in fig.axes], visible=True, fontsize=5)
     
    # Choose layout spacing
    plt.tight_layout(pad=2, w_pad=0.0, h_pad=1.0)
#         plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=None, hspace=None)
    # Set top title
    plt.suptitle("Forecast: " + title, y=1.0)
    # save to pdf
    from matplotlib.backends.backend_pdf import PdfPages
    pp = PdfPages('./results/forecast_'+title+'.pdf')
    plt.savefig(pp, format='pdf')
    pp.close()
#     show to desktop, call is blocking
    plt.show()

def print_ts(data, title=None):
    fig, axes = plt.subplots(nrows=1, ncols=1, sharex=False, sharey=False, figsize=(12,10))
    plt.rcParams.update(RCPARAMS)
    data.plot.line(ax=axes)
    
    fig.autofmt_xdate()
    # set font size
    plt.setp([a.get_xticklabels() for a in fig.axes], visible=True, fontsize=5)
    plt.setp([a.get_yticklabels() for a in fig.axes], visible=True, fontsize=5)
     
    # Choose layout spacing
    plt.tight_layout(pad=2, w_pad=0.0, h_pad=1.0)
#         plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=None, hspace=None)
    # Set top title
    plt.suptitle(title, y=1.0)
    # save to pdf
    from matplotlib.backends.backend_pdf import PdfPages
    pp = PdfPages('./results/' + title + '.pdf')
    plt.savefig(pp, format='pdf')
    pp.close()
#     show to desktop, call is blocking
#     plt.show()

def forecast_arma(data, title):
    assert (data.shape[0])
    
    model = sm.tsa.ARMA(data[0].values, order=(0,1))
    results = model.fit(disp=-1)  
    data[1] = results.fittedvalues
    # Check model result 
    '''
    The Durbin Watson statistic is a number that tests for autocorrelation in the residuals 
    from a statistical regression analysis. The Durbin-Watson statistic is always between 0 and 4.
    A value of 2 means that there is no autocorrelation in the sample. Values approaching 0 
     indicate positive autocorrelation and values toward 4 indicate negative autocorrelation.
    '''
    
    durbin_watson = sm.stats.durbin_watson(results.resid)
    print ("\nDurbin-watson=%0.8f" % (durbin_watson))
    # the lower the better
    print ("AIC=%0.8f" % results.aic)
    print ("BIC=%0.8f" % results.bic)
    rss = np.sum((results.fittedvalues - data[0])**2)
    print ("RSS=%.08f" % (rss)) 
    rmse = sm.tools.eval_measures.rmse(data[0].values, data[1].values)
    print ("RMSE=%.08f\n" % (rmse))
    
    mp.Process(target=print_forecast, args=(data[[0, 1]], title)).start()
    
    resid = pd.DataFrame(data=results.resid)
    mp.Process(target=print_ts, args=(resid, title +"_residual")).start()
    
    

def main():
    '''
    Constructs dataframe ts from csv file
    '''
    print('readfile %s' % FILEPATH)
    raw_data = None
    headers_ = ['Datetime','Open','High','Low','Close', 'Volume']
    # oanda dataset uses first 2 columns for data and time separately
    parse_dates_ = [[0,1]]
    try:
        dateparser = lambda x,y: pd.datetime.strptime(x+" "+y, '%Y.%m.%d %H:%M')
        raw_data = pd.read_csv(FILEPATH, 
                     delimiter=',', 
                     header=None, 
                     parse_dates= parse_dates_,
                     date_parser=dateparser)
        
        raw_data.columns = headers_
        raw_data.set_index(['Datetime'], drop=True, inplace=True)
    except:
        print ("Exception reading source file")

    print raw_data.info()
    print raw_data.head(10)  
    
    
    
    '''
    2. Start ts processing
    '''
    '''
    Pre-process: test stationarity
    '''
#     mp.Process(target=test_stationarity, args=(data['Close'], 'Close_PreProcess', 500)).start()
    
    '''
    Remove Trend: Differencing
    '''
    data = pd.DataFrame(raw_data['Close'], index=raw_data.index, columns=['Close'])
    data['Log'] = np.log(data['Close'])
    data['Diff'] = data['Close'] - data['Close'].shift(1)
    data['LogDiff'] = data['Log'] - data['Log'].shift(1)
#     print diff['Diff_1'].head(10)
    data.dropna(inplace=True)
    mp.Process(target=test_stationarity, args=(data['Diff'], 'Diff', 365*2)).start()
    mp.Process(target=test_autocorrelation, args=(data['Diff'], 'Diff', 60)).start()
    
    mp.Process(target=test_stationarity, args=(data['LogDiff'], 'LogDiff', 365*2)).start()
    mp.Process(target=test_autocorrelation, args=(data['LogDiff'], 'LogDiff', 60)).start()
    
    '''
    Remove Seasonality
    '''
    
    '''
    Perform ARIMA forecast
    '''
    forecast = pd.DataFrame(data=data['LogDiff'].values, index=data.index)
    mp.Process(target=forecast_arma, args=(forecast, 'LogDiff')).start()
    
#     model = sm.tsa.ARIMA(diff['Diff_1'], order=(0, 0, 1))  
#     model = sm.tsa.ARMA(data['Diff'], order=(0,1))
#     results = model.fit(disp=-1)  
#     data['Forecast'] = results.fittedvalues
#     # Check model result 
#     '''
#     The Durbin Watson statistic is a number that tests for autocorrelation in the residuals 
#     from a statistical regression analysis. The Durbin-Watson statistic is always between 0 and 4.
#     A value of 2 means that there is no autocorrelation in the sample. Values approaching 0 
#      indicate positive autocorrelation and values toward 4 indicate negative autocorrelation.
#     '''
#     durbin_watson = sm.stats.durbin_watson(results.resid.values)
#     print ("\nDurbin-watson=%0.8f" % (durbin_watson))
#     # the lower the better
#     print ("AIC=%0.8f" % results.aic)
#     print ("BIC=%0.8f" % results.bic)
#     rss = np.sum((results.fittedvalues - data['Diff'])**2)
#     print ("RSS=%.08f" % (rss)) 
#     rmse = sm.tools.eval_measures.rmse(data['Diff'].values, data['Forecast'].values)
#     print ("RMSE=%.08f" % (rmse))
#     mp.Process(target=print_forecast, args=(data[['Diff', 'Forecast']], )).start()
#     resid = pd.DataFrame(data=results.resid.values)
#     mp.Process(target=print_ts, args=(resid, "arma_residual")).start()
    
if __name__=='__main__':
    main()