import os  
import numpy as np  
import pandas as pd  
import matplotlib.pyplot as plt  
import statsmodels.api as sm  
from statsmodels.tsa.stattools import adfuller
from pandas.tools.plotting import autocorrelation_plot
import seaborn as sb  
import multiprocessing as mp
from scipy import stats
import datetime


# References:
# http://www.johnwittenauer.net/a-simple-time-series-analysis-of-the-sp-500-index/
# http://statsmodels.sourceforge.net/devel/examples/notebooks/generated/tsa_arma.html
# https://www.otexts.org/fpp/8/1
# https://www.analyticsvidhya.com/blog/2016/02/time-series-forecasting-codes-python/

# sb.set_style('darkgrid')
# path = os.getcwd() + '\data\stock_data.csv'  
# stock_data = pd.read_csv(path)  
# stock_data['Date'] = stock_data['Date'].convert_objects(convert_dates='coerce')  
# stock_data = stock_data.sort_index(by='Date')  
# stock_data = stock_data.set_index('Date')  

TRADINGDAYS = 252 # https://en.wikipedia.org/wiki/Trading_day
FILEPATH = '/home/akalingking/dataset/oanda/EURUSD1440.csv'
PLTFONTSIZE = 8
RCPARAMS = {'axes.titlesize':PLTFONTSIZE,
             'axes.labelsize':PLTFONTSIZE,
             'legend.fontsize':PLTFONTSIZE,
             'xtick.labelsize':PLTFONTSIZE,
             'ytick.labelsize':PLTFONTSIZE,}

def test_stationarity(data, title, window):
    rollingMean = pd.Series.rolling(data, window=window).mean()
    rollingStd = pd.Series.rolling(data, window=window).std()
    
    # Dickey-Fuller Test
    # Test statistic must be smaller thatn the crititical value
    # H_0 == Has Unit-Root (not stationary)
    # H_1 == Has no Unit-Root (stationary)
    # if test-statistic is smalller, we reject the null hypthesis
    # Alternatively, if p-value is less than 0.05 reject the null hypothesis
    print ('\nTimeseries - %s:' % title)
    print 'Dickey-Fuller Test:'
#     print data.head(10)
    dftest = adfuller(data.values, autolag='AIC')
    dfoutput = pd.Series(dftest[0:4], index=['Test Statistic','p-value','#Lags Used','Number of Observations Used'])
    for key,value in dftest[4].items():
        dfoutput['Critical Value (%s)'%key] = value
    print dfoutput
    print '\n'
    
    s = pd.DataFrame(data=data, index=data.index, columns=['Original'])
    s['Mean'] = rollingMean
    s['Stdev'] = rollingStd
    
    fig, axes = plt.subplots(nrows=6, ncols=1, sharex=False, sharey=False, figsize=(10,15))
    plt.rcParams.update(RCPARAMS)
    s.plot(ax=axes[0], title='Data', legend='best')
    
    rollingMean.plot.line(ax=axes[1], color='blue', title="Rolling Mean");
    rollingStd.plot.line(ax=axes[2], color='red', title='Rolling Std');
    
    '''
    Decompose
    '''
    decomposition = sm.tsa.seasonal_decompose(data, model='additive', freq=window)  
    decomposition.trend.plot(ax=axes[3], title='Trend')
    decomposition.seasonal.plot(ax=axes[4], title='Seasonal')
    decomposition.resid.plot(ax=axes[5], title='Residual')
    
    '''
    Show plots
    '''
    fig.autofmt_xdate()
    # set font size
    plt.setp([a.get_xticklabels() for a in fig.axes], visible=True, fontsize=PLTFONTSIZE)
    plt.setp([a.get_yticklabels() for a in fig.axes], visible=True, fontsize=PLTFONTSIZE)
    
    # Choose layout spacing
    plt.tight_layout(pad=3, w_pad=0.0, h_pad=2.0)
#         plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=None, hspace=None)
    # Set top title
    plt.suptitle(title, y=1.0)
    # save to pdf
    from matplotlib.backends.backend_pdf import PdfPages
    pp = PdfPages('./results/descriptive_' + title + '.pdf')
    plt.savefig(pp, format='pdf')
    pp.close()
    #show to desktop, call is blocking
#     plt.show()

def test_autocorrelation(data, title, window=40, ymax=0.05):
    from statsmodels.tsa import stattools
    # Prepare figure    
    fig, axes = plt.subplots(nrows=5, ncols=1, sharex=False, sharey=False, figsize=(10,15))
    plt.rcParams.update(RCPARAMS)
    # plot the original data
    data.plot.line(ax=axes[0], title=title)
    
    acf_ = stattools.acf(data, nlags=window) #actual data
    sm.graphics.tsa.plot_acf(data.values.squeeze(), lags=window, ax=axes[1])
    ymax = np.sort(acf_)[-2] *1.5
    axes[1].set_ylim(-ymax, ymax)
    
    pacf_ = stattools.pacf(data, nlags=window) # actual data
    sm.graphics.tsa.plot_pacf(data, lags=window, ax=axes[2])
    ymax = np.sort(pacf_)[-2] *1.5 
    axes[2].set_ylim(-ymax, ymax)
     
#     autocorrelation_plot(data, ax=axes[3])
#     axes[3].set_ylim(np.min(data), np.max(data))

    
    #altrenative, manual?
    # http://statsmodels.sourceforge.net/0.6.0/generated/statsmodels.tsa.stattools.pacf.html
    z = 1.96#stats.norm.ppf(0.95)
    ax = axes[3]
    pacf = stattools.pacf(data, nlags=window, method='ols')
    pacf_ = pd.DataFrame(data=pacf, columns=['PACF'])
    pacf_.plot.line(ax=ax, title='PACF', label=None)
    
    axline = z / np.sqrt(pacf_.shape[0])
    c = '#660099'
    ax.set_ylim(-axline * 1.1, axline * 1.1)
    ax.axhline(y=axline, linestyle='--', color=c)
    ax.axhline(y=-axline, linestyle='--', color=c)
    
    ax = axes[4]
    acf = stattools.acf(data, nlags=window)
    acf_ = pd.DataFrame(data=acf, columns=['ACF'])
    acf_.plot.line(ax=ax, title='ACF', label=None)
    axline = z / np.sqrt(acf_.shape[0])
    c = '#660099'
    ax.set_ylim(-axline * 1.1, axline * 1.1)
    ax.axhline(y=axline, linestyle='--', color=c)
    ax.axhline(y=-axline, linestyle='--', color=c)
    
    '''
    Show plots
    '''
    fig.autofmt_xdate()
    # set font size
    plt.setp([a.get_xticklabels() for a in fig.axes], visible=True, fontsize=PLTFONTSIZE)
    plt.setp([a.get_yticklabels() for a in fig.axes], visible=True, fontsize=PLTFONTSIZE)
    
    # Choose layout spacing
    plt.tight_layout(pad=3, w_pad=0.0, h_pad=2.0)
#         plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=None, hspace=None)
    # Set top title
    plt.suptitle(title, y=1.0)
    # save to pdf
    from matplotlib.backends.backend_pdf import PdfPages
    pp = PdfPages('./results/correlation_' + title + '.pdf')
    plt.savefig(pp, format='pdf')
    pp.close()
    #show to desktop, call is blocking
#     plt.show()

def print_ts(data, title=None):
    fig, axes = plt.subplots(nrows=1, ncols=1, sharex=False, sharey=False, figsize=(12,10))
    plt.rcParams.update(RCPARAMS)
    data.plot.line(ax=axes)
    
    fig.autofmt_xdate()
    # set font size
    plt.setp([a.get_xticklabels() for a in fig.axes], visible=True, fontsize=5)
    plt.setp([a.get_yticklabels() for a in fig.axes], visible=True, fontsize=5)
     
    # Choose layout spacing
    plt.tight_layout(pad=2, w_pad=0.0, h_pad=1.0)
#         plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=None, hspace=None)
    # Set top title
    plt.suptitle(title, y=1.0)
    # save to pdf
    from matplotlib.backends.backend_pdf import PdfPages
    pp = PdfPages('./results/' + title + '.pdf')
    plt.savefig(pp, format='pdf')
    pp.close()
#     show to desktop, call is blocking
#     plt.show()

def forecast_arma(x, title, order):
    assert (x.shape[0])
    model = sm.tsa.ARMA(x, order=order)
    results = model.fit(disp=-1)
      
    y = results.fittedvalues
    
    # Check model result 
    '''
    The Durbin Watson statistic is a number that tests for autocorrelation in the residuals 
    from a statistical regression analysis. The Durbin-Watson statistic is always between 0 and 4.
    A value of 2 means that there is no autocorrelation in the sample. Values approaching 0 
     indicate positive autocorrelation and values toward 4 indicate negative autocorrelation.
    '''
    durbin_watson = sm.stats.durbin_watson(results.resid)
    # the lower aic and bic the better
    rss = np.sum((y - x)**2)
    rmse = sm.tools.eval_measures.rmse(x.values, y.values)
    print('\nARMA results for %s\nDurbin-Watson:\t%0.8f\naic:\t\t%0.8f\nbic:\t\t%0.8f\nrss:\t\t%0.8f\nrmse:\t\t%0.8f\n' % 
          (title, durbin_watson, results.aic, results.bic, rss, rmse))
    
    # print dataframe
    df = pd.DataFrame(data={'x':x.values, 'y':y.values}, index=x.index)
    mp.Process(target=print_ts, args=(df, 'arma_'+title)).start()
     
    '''
    Validate residual is stationary
    '''
    resid = pd.DataFrame(data=results.resid)
    mp.Process(target=print_ts, args=(resid, "arma_residual_"+title)).start()
    
    # TODO: change args to Series where only one column is enough
    #test stationarity
#     mp.Process(target=test_stationarity, args=(resid.iloc[0, :], 'arma_resid_'+title, TRADINGDAYS)).start()
    # test residual correlation
    mp.Process(target=test_autocorrelation, args=(resid, 'arma_resid_'+title)).start()
    
    #TODO: https://www.analyticsvidhya.com/blog/2016/02/time-series-forecasting-codes-python/
    '''
    Predict future values
    '''
    print 'tail'
    print x.tail(10)
    pred = results.predict(start=datetime.'2017-02-08', end='2017-02-15')
    print pred
    
    
def forecast_arima(x, title, order):
    assert (x.shape[0])
    model = sm.tsa.ARIMA(x, order=order)
    results = model.fit(disp=-1)  
    y = results.fittedvalues
    
    x_ = x
    # Check for diff and adjust x-data
    if (order[1] != 0):
        x_ = x[order[1]:]
        x = x_
        
    # Check model result 
    '''
    The Durbin Watson statistic is a number that tests for autocorrelation in the residuals 
    from a statistical regression analysis. The Durbin-Watson statistic is always between 0 and 4.
    A value of 2 means that there is no autocorrelation in the sample. Values approaching 0 
     indicate positive autocorrelation and values toward 4 indicate negative autocorrelation.
    '''
    durbin_watson = sm.stats.durbin_watson(results.resid)
    # the lower aic and bic the better
    rss = np.sum((y - x)**2)
    rmse = sm.tools.eval_measures.rmse(x.values, y.values)
    print('\nARIMA results for %s\nDurbin-Watson:\t%0.8f\naic:\t\t%0.8f\nbic:\t\t%0.8f\nrss:\t\t%0.8f\nrmse:\t\t%0.8f\n' % 
          (title, durbin_watson, results.aic, results.bic, rss, rmse))
    
    # print dataframe
    df = pd.DataFrame(data={'x':x.values, 'y':y.values}, index=x.index)
    mp.Process(target=print_ts, args=(df, 'arima_'+title)).start()
     
    resid = pd.DataFrame(data=results.resid)
    mp.Process(target=print_ts, args=(resid, "arima_residual_"+title)).start()
    mp.Process(target=test_autocorrelation, args=(resid, 'arima_resid_'+title)).start()
    
    

def main():
    '''
    Constructs dataframe ts from csv file
    '''
    print('readfile %s' % FILEPATH)
    raw_data = None
    headers_ = ['Datetime','Open','High','Low','Close', 'Volume']
    # oanda dataset uses first 2 columns for data and time separately
    parse_dates_ = [[0,1]]
    try:
        dateparser = lambda x,y: pd.datetime.strptime(x+" "+y, '%Y.%m.%d %H:%M')
        raw_data = pd.read_csv(FILEPATH, 
                     delimiter=',', 
                     header=None, 
                     parse_dates= parse_dates_,
                     date_parser=dateparser)
        
        raw_data.columns = headers_
        raw_data.set_index(['Datetime'], drop=True, inplace=True)
    except:
        print ("Exception reading source file")

    print raw_data.info()
    print raw_data.head(10)  
    
    
    
    '''
    2. Start ts processing
    '''
    '''
    Pre-process: test stationarity
    https://www.otexts.org/fpp/8/1
    '''
    close = pd.Series(data=raw_data['Close'].values, index=raw_data.index) 
    mp.Process(target=test_stationarity, args=(close, 'Close', TRADINGDAYS)).start()
    mp.Process(target=test_autocorrelation, args=(close, 'Close', 40)).start()
    
    '''
    Remove Trend: Differencing
    '''
    data = pd.DataFrame(raw_data['Close'], index=raw_data.index, columns=['Close'])
    data['Log'] = np.log(data['Close'])
    data['Diff'] = data['Close'] - data['Close'].shift(1)
    data['LogDiff'] = data['Log'] - data['Log'].shift(1)
    
    data['Diff2'] = data['Close'] - data['Close'].shift(2)

    # Create Timeseries variables
    close = pd.Series(data=data['Close'].values, index=data.index)
    
    # Remove trend
    log = pd.Series(data=data['Log'].values, index=data.index)
    log.dropna(inplace=True)
    
    # Remove seaonality
    diff = pd.Series(data=data['Diff'].values, index=data.index)
    diff.dropna(inplace=True)
    
    logdiff = pd.Series(data=data['LogDiff'].values, index=data.index)
    logdiff.dropna(inplace=True)
    
    diff2 = pd.Series(data['Diff2'].values, index=data.index)
    diff2.dropna(inplace=True)

    '''
    Test Stationarity
    '''
    mp.Process(target=test_stationarity, args=(diff2, 'Diff2', TRADINGDAYS)).start()
    
    '''
    Tesr Autocorrelation
    ''' 
#     mp.Process(target=test_autocorrelation, args=(close, 'Close', 40)).start()
#     mp.Process(target=test_autocorrelation, args=(diff, 'Diff', 40)).start()
#     mp.Process(target=test_autocorrelation, args=(log, 'Log', 40)).start()
#     mp.Process(target=test_autocorrelation, args=(logdiff, 'LogDiff', 40)).start()
    mp.Process(target=test_autocorrelation, args=(diff2, 'Diff2', 40)).start()
      

    '''
    Perform ARIMA forecast
    '''
#     mp.Process(target=forecast_arma, args=(log, 'Log')).start()
#     mp.Process(target=forecast_arma, args=(diff, 'Diff')).start()
    mp.Process(target=forecast_arma, args=(diff2, 'Diff2', (2,1))).start()
#     mp.Process(target=forecast_arima, args=(logdiff, 'Diff', (1,1,0))).start()
    
    
if __name__=='__main__':
    main()