import os  
import numpy as np  
import pandas as pd  
import matplotlib.pyplot as plt  
import statsmodels.api as sm  
from statsmodels.tsa.stattools import adfuller
import multiprocessing as mp
from scipy import stats
from datetime import datetime
import statsmodels

'''
History:
[1] Add Forecast function
'''
# References:
# http://www.johnwittenauer.net/a-simple-time-series-analysis-of-the-sp-500-index/
# http://statsmodels.sourceforge.net/devel/examples/notebooks/generated/tsa_arma.html
# https://www.otexts.org/fpp/8/1
# https://www.analyticsvidhya.com/blog/2016/02/time-series-forecasting-codes-python/
# http://conference.scipy.org/proceedings/scipy2015/pdfs/margaret_mahan.pdf


CORR_LAG = 40
TRADINGDAYS = 252 # https://en.wikipedia.org/wiki/Trading_day
FILEPATH = '/home/akalingking/dataset/oanda/EURUSD1440.csv'
PLTFONTSIZE = 8
RCPARAMS = {'axes.titlesize':PLTFONTSIZE,
             'axes.labelsize':PLTFONTSIZE,
             'legend.fontsize':PLTFONTSIZE,
             'xtick.labelsize':PLTFONTSIZE,
             'ytick.labelsize':PLTFONTSIZE,}

def print_ts(data, title=None):
    '''
    @param data datafram instance
    '''
    fig, axes = plt.subplots(nrows=1, ncols=1, sharex=False, sharey=False, figsize=(12,10))
    plt.rcParams.update(RCPARAMS)
    data.plot.line(ax=axes)
    
    fig.autofmt_xdate()
    # set font size
    plt.setp([a.get_xticklabels() for a in fig.axes], visible=True, fontsize=5)
    plt.setp([a.get_yticklabels() for a in fig.axes], visible=True, fontsize=5)
     
    # Choose layout spacing
    plt.tight_layout(pad=2, w_pad=0.0, h_pad=1.0)
#         plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=None, hspace=None)
    # Set top title
    plt.suptitle(title, y=1.0)
    # save to pdf
    from matplotlib.backends.backend_pdf import PdfPages
    pp = PdfPages('./results/' + title + '.pdf')
    plt.savefig(pp, format='pdf')
    pp.close()
#     show to desktop, call is blocking
#     plt.show()

def test_stationarity(data, title, window):
    '''
    @param data    series
    @param title   name of series
    @param window  lag  
    '''
    
    '''
   [1] Unit-Root Test
    '''
    # Augmented Dickey-Fuller Test
    # Test statistic must be smaller thatn the crititical value
    # H_0 == Has Unit-Root (not stationary)
    # H_1 == Has no Unit-Root (stationary)
    # if test-statistic is smalller, we reject the null hypthesis
    # Alternatively, if p-value is less than 0.05 reject the null hypothesis
    # http://statsmodels.sourceforge.net/devel/generated/statsmodels.tsa.stattools.adfuller.html
    dftest = adfuller(data.values, autolag='AIC')
    #@{print result to console
    critical_values = 'critical values:\n'
    for key,value in dftest[4].items():
        critical_values += '(%s)\t\t%0.8f\n' % (key,value)
    print ('\nADF Test:\t%s\n'
           't-statistic\t%.8f\n'
           'p-value\t\t%.8f\n'
           'lags used\t%d\n'
           'noobs\t\t%d\n'
           '%s\n' % (title, dftest[0], dftest[1], dftest[2], dftest[3], critical_values))
    #@}
    
    '''
    [2] Variables for stationarity plots
    '''
    rollingMean = pd.Series.rolling(data, window=window).mean()
    rollingStd = pd.Series.rolling(data, window=window).std()
    s = pd.DataFrame(data=data, index=data.index, columns=['Original'])
    s['Rolling-Mean'] = rollingMean
    s['Rolling-Stdev'] = rollingStd
    
    '''
    [3] Decompose timeseries
    '''
    composition = sm.tsa.seasonal_decompose(data, model='additive', freq=window)

    '''
    Show plots
    '''    
    fig, axes = plt.subplots(nrows=4, ncols=1, sharex=False, sharey=False, figsize=(10,15))
    plt.rcParams.update(RCPARAMS)
    
    #{@ Plot
    s.plot(ax=axes[0], title='Data', legend='best')
    composition.trend.plot(ax=axes[1], title='Trend')
    composition.seasonal.plot(ax=axes[2], title='Seasonal')
    composition.resid.plot(ax=axes[3], title='Residual')
    #}@plot

    fig.autofmt_xdate()
    # set font size
    plt.setp([a.get_xticklabels() for a in fig.axes], visible=True, fontsize=PLTFONTSIZE)
    plt.setp([a.get_yticklabels() for a in fig.axes], visible=True, fontsize=PLTFONTSIZE)
    
    # Choose layout spacing
    plt.tight_layout(pad=3, w_pad=0.0, h_pad=2.0)
#         plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=None, hspace=None)
    # Set top title
    plt.suptitle(title, y=1.0)
    # save to pdf
    from matplotlib.backends.backend_pdf import PdfPages
    pp = PdfPages('./results/stationarity_' + title + '.pdf')
    plt.savefig(pp, format='pdf')
    pp.close()
    #show to desktop, call is blocking
#     plt.show()
    

def test_autocorrelation(data, title, window=40, ymax=0.05):
    from statsmodels.tsa import stattools
    from statsmodels.graphics import gofplots
    '''
    Show plots
    '''
    # Prepare figure    
    fig, axes = plt.subplots(nrows=4, ncols=1, sharex=False, sharey=False, figsize=(10,15))
    plt.rcParams.update(RCPARAMS)
    # plot the original data
    data.plot.line(ax=axes[0], title=title)
    
    #{ plot
    acf_ = stattools.acf(data, nlags=window) # actual data, just to get range of diplay
    sm.graphics.tsa.plot_acf(data.values.squeeze(), lags=window, ax=axes[1])
    ymax = np.sort(acf_)[-2] *1.5
    axes[1].set_ylim(-ymax, ymax)
    
    pacf_ = stattools.pacf(data, nlags=window) # actual data, just to get range of diplay
    sm.graphics.tsa.plot_pacf(data, lags=window, ax=axes[2])
    ymax = np.sort(pacf_)[-2] *1.5 
    axes[2].set_ylim(-ymax, ymax)
    
    # http://statsmodels.sourceforge.net/devel/generated/statsmodels.graphics.gofplots.qqplot.html
    sm.qqplot(data=data, ax=axes[3], line='r')
    #} plot
    
    fig.autofmt_xdate()
    # set font size
    plt.setp([a.get_xticklabels() for a in fig.axes], visible=True, fontsize=PLTFONTSIZE)
    plt.setp([a.get_yticklabels() for a in fig.axes], visible=True, fontsize=PLTFONTSIZE)
    # Choose layout spacing
    plt.tight_layout(pad=3, w_pad=0.0, h_pad=2.0)
#         plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=None, hspace=None)
    # Set top title
    plt.suptitle('Correlation: ' + title, y=1.0)
    # save to pdf
    from matplotlib.backends.backend_pdf import PdfPages
    pp = PdfPages('./results/correlation_' + title + '.pdf')
    plt.savefig(pp, format='pdf')
    pp.close()
    #show to desktop, call is blocking
#     plt.show()

def predict_arma(x, title, order):
    '''
    [1] Train
    '''
    assert (x.shape[0])
    model = sm.tsa.ARMA(x, order=order)
    result = model.fit(disp=-1)
    y = result.fittedvalues
    resid = result.resid
        
    '''
    [2] Validate model
    '''
    '''
    [3] Test for serial correlation
    ''' 
    # https://www.r-bloggers.com/story-of-the-ljung-box-blues-progress-not-perfection/
    # http://www.itl.nist.gov/div898/handbook/pmc/section4/pmc4481.htm
    # Ljung-box requires input to be stationary and has a mean of 0.
    # Test normality
    # Not applicable to large dataset
    #tstat, pval = stats.shapiro(data)
    # H0: The data are independently distributed (i.e. the correlations in the population 
    #     from which the sample is taken are 0, so that any observed correlations in the 
    #     data result from randomness of the sampling process).
    # Ha: The data are not independently distributed; they exhibit serial correlation.   
    from statsmodels.stats import diagnostic
    lb = diagnostic.acorr_ljungbox(resid, lags=40, boxpierce=False)
    # p-values must be greater than critical values eg 0.05
    lb_pass = False
    pvals = lb[1]
    rejection_region = pvals[pvals < 0.05]
    if len(rejection_region) <= 0:
        lb_pass = True
        
    if (lb_pass):
        print ("%s arma%s residual is independently distributed" % (title, order))
    else:
        print ("%s arma%s residual is NOT independently distributed" % (title, order))
        # we should fail here, but proceed anyway
        
    '''
    [4] Validate residual is stationary
    
    The Durbin Watson statistic is a number that tests for autocorrelation in the residuals 
    from a statistical regression analysis. The Durbin-Watson statistic is always between 0 and 4.
    A value of 2 means that there is no autocorrelation in the sample. Values approaching 0 
    indicate positive autocorrelation and values toward 4 indicate negative autocorrelation.
    Note: Durbin-watson result is compromised if the model uses AR component
    http://stats.stackexchange.com/questions/154167/why-ever-use-durbin-watson-instead-of-testing-autocorrelation 
    '''
    durbin_watson = sm.stats.durbin_watson(resid)
    # the lower aic and bic the better
    rss = np.sum((y - x)**2)
    rmse = sm.tools.eval_measures.rmse(x.values, y.values)
    print('\nARMA results for %s\nDurbin-Watson:\t%0.8f\naic:\t\t%0.8f\nbic:\t\t%0.8f\nrss:\t\t%0.8f\nrmse:\t\t%0.8f\n' % 
          (title, durbin_watson, result.aic, result.bic, rss, rmse))
    
    #{@print results
    df = pd.DataFrame(data={'x':x.values, 'y':y.values}, index=x.index)
    mp.Process(target=print_ts, args=(df, 'arma_' + title)).start()
    #print residual
    resid_ = pd.DataFrame(data=resid)
    mp.Process(target=print_ts, args=(resid_, "arma_residual_"+title)).start()
    mp.Process(target=test_stationarity, args=(resid, 'arma_resid_'+title, TRADINGDAYS)).start()
    mp.Process(target=test_autocorrelation, args=(resid_, 'arma_resid_'+title)).start()
    #}@print results
    return result
    
    
def forecast_arma(x, result, steps=1):
    '''
    @param x
    @type x: pandas.Series
    @param result
    @type result: ARMAResult
    @param steps
    @return Series
    '''
    from pandas.tseries.offsets import BDay
    '''
    Predict future values
    '''
    #TODO: https://groups.google.com/forum/#!topic/pystatsmodels/_ItLBVpePIY
    # http://machinelearningmastery.com/time-series-forecast-uncertainty-using-confidence-intervals-python/
#     print 'tail'
#     print x.tail(10)
    dates = []
#     dates.append(datetime.strptime('2017-02-06','%Y-%m-%d'))

    for i in xrange(0, steps):
        newdate = (x.index[-1] + BDay(i)).to_pydatetime()
        dates.append( newdate)
        
    forecast = pd.Series(np.zeros(np.size(dates)),index=dates)
#     test.index.asfreq('D')
    # use forecast to predict out sample data
    pred, sterr, confint= result.forecast(steps=steps)
    print 'Forecast Result:'
    print 'Predicted:\n',pred
    print 'Std error:\n', sterr
    print 'Confidence Interval:\n', confint
    
    # Package return var
    for i in xrange(0, steps):
        forecast[i] = pred[i]

    return forecast
    
    
    

def main():
    '''
    Constructs dataframe ts from csv file
    '''
    print('readfile %s' % FILEPATH)
    raw_data = None
    headers_ = ['Datetime','Open','High','Low','Close', 'Volume']
    # oanda dataset uses first 2 columns for data and time separately
    parse_dates_ = [[0,1]]
    try:
        dateparser = lambda x,y: pd.datetime.strptime(x+" "+y, '%Y.%m.%d %H:%M')
        raw_data = pd.read_csv(FILEPATH, 
                     delimiter=',', 
                     header=None, 
                     parse_dates= parse_dates_,
                     date_parser=dateparser)
        
        raw_data.columns = headers_
        raw_data.set_index(['Datetime'], drop=True, inplace=True)
    except:
        print ("Exception reading source file")

    print raw_data.info()
#     print raw_data.head(10)  
    
    
    
    '''
    2. Start ts processing
    '''
    '''
    Pre-process: test stationarity
    https://www.otexts.org/fpp/8/1
    '''
    close = pd.Series(data=raw_data['Close'].values, index=raw_data.index) 
    mp.Process(target=test_stationarity, args=(close, 'Close', TRADINGDAYS)).start()
    mp.Process(target=test_autocorrelation, args=(close, 'Close', 40)).start()
    
    '''
    Remove Trend: Differencing
    '''
    data = pd.DataFrame(raw_data['Close'], index=raw_data.index, columns=['Close'])
    data['Log'] = np.log(data['Close'])
    data['Diff'] = data['Close'] - data['Close'].shift(1)
    data['LogDiff'] = data['Log'] - data['Log'].shift(1)
    
    data['Diff2'] = data['Close'] - data['Close'].shift(2)

    # Create Timeseries variables
    close = pd.Series(data=data['Close'].values, index=data.index)
    
    # Remove trend
    log = pd.Series(data=data['Log'].values, index=data.index)
    log.dropna(inplace=True)
    
    # Remove seaonality
    diff = pd.Series(data=data['Diff'].values, index=data.index)
    diff.dropna(inplace=True)
    
    logdiff = pd.Series(data=data['LogDiff'].values, index=data.index)
    logdiff.dropna(inplace=True)
    
    diff2 = pd.Series(data['Diff2'].values, index=data.index)
    diff2.dropna(inplace=True)

    '''
    Test Stationarity
    '''
    mp.Process(target=test_stationarity, args=(diff2, 'Diff2', TRADINGDAYS)).start()
    
    '''
    Test Autocorrelation
    ''' 
#     mp.Process(target=test_autocorrelation, args=(close, 'Close', 40)).start()
#     mp.Process(target=test_autocorrelation, args=(diff, 'Diff', 40)).start()
#     mp.Process(target=test_autocorrelation, args=(log, 'Log', 40)).start()
#     mp.Process(target=test_autocorrelation, args=(logdiff, 'LogDiff', 40)).start()
    mp.Process(target=test_autocorrelation, args=(diff2, 'Diff2', 40)).start()
      

    '''
    Perform ARIMA forecast
    '''
#     mp.Process(target=forecast_arma, args=(log, 'Log')).start()
#     mp.Process(target=forecast_arma, args=(diff, 'Diff')).start()
#     mp.Process(target=forecast_arma, args=(diff2, 'Diff2', (2,1))).start()
#     mp.Process(target=forecast_arima, args=(logdiff, 'Diff', (1,1,0))).start()
    result = predict_arma(diff2, 'Diff2', (2,1))
    if result is not None:
        print result.summary()
        forecast = forecast_arma(diff2, result, steps=5)
        print forecast
    else:
        print "Failed to generate ARMA Model"
    
if __name__=='__main__':
    main()