import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
import statsmodels.api as sm
import matplotlib
import multiprocessing as mp
import os
from pandas.tools.plotting import autocorrelation_plot
#import datetime
#import threading
#from scipy.stats import norm
#DATAPATH = '/home/akalingking/dataset/oanda/'

PLTFONTSIZE = 8
RCPARAMS = {'axes.titlesize':PLTFONTSIZE,
             'axes.labelsize':PLTFONTSIZE,
             'legend.fontsize':PLTFONTSIZE,
             'xtick.labelsize':PLTFONTSIZE,
             'ytick.labelsize':PLTFONTSIZE,}

class OhlcDescStat:
    headers_ = ['Datetime','Open','High','Low','Close', 'Volume']
    # oanda dataset uses first 2 columns for data and time separately
    parse_dates_ = [[0,1]]
    
    def __init__(self, filepath, async=True):
        self.filepath_ = filepath
        _,tail = os.path.split(filepath)       
        self.name_ = tail.split('.')[0]

        data = self.read_file(filepath)
        print data.shape
        if (data is not None and data.shape[0] > 0):
            args = []
            
            args.append((data['Close'], 'Close'))
            # handle returns
            returns = data['Close'].diff()/data['Close'].shift()
            #set NaN to numeric
            returns[0] = 0 
            args.insert(0,(returns, 'Returns'))
            #print returns.head(10)
            
            args.reverse()
            for i in args:
                if (async):
                    #threading.Thread(target=self.process, args=i).start()
                    mp.Process(target=self.process, args=i).start()
                else:
                    self.process(i[0], i[1])
                
        
    def read_file(self, filepath):
        '''
        Constructs dataframe ts from csv file
        '''
        print('readfile %s' % filepath)
        data = None
        try:
            dateparser = lambda x,y: pd.datetime.strptime(x+" "+y, '%Y.%m.%d %H:%M')
            data = pd.read_csv(filepath, 
                         delimiter=',', 
                         header=None, 
                         parse_dates = self.parse_dates_,
                         date_parser = dateparser)
            
            data.columns = self.headers_
            data.set_index(['Datetime'],drop=True, inplace=True)
            print data.info()
            print data.head(10)    
        except:
            print ("Exception reading source file")
            
        return data
    
    
    def process(self, data, title=None):
        '''
        1. Constructs descriptive stats and visualization
        '''
        mean = data.mean()
        std = data.std()
        median = data.median()
        kurt = data.kurt()
        skew = data.skew()
        max_ = np.max(data)
        min_ = np.min(data)
        mode = data.mode()
        # mode can be non-scaler
        if not np.isscalar(mode):
            print "Miltiple modes detected"
            #print "mode: ", mode
            if np.size(mode) <= 0:
                print "No mode generated, assigning 0"
                mode = 0
            else:
                print "selecting first value"
                mode = mode[0]
        print "Mode: ", mode
        
        fig, axes = plt.subplots(nrows=6, ncols=1, sharex=False, sharey=False, figsize=(10,15))
        plt.rcParams.update(RCPARAMS)
       
        data.plot.line(y=data[1], x=data.index.values, ax=axes[0], title=title, legend=None)
     
        data.plot.kde(ax=axes[1], title='Density')
        axes[1].set_xlim(min_, max_)
#         axes[1].xaxis.set_xticks(np.arange(min, max, .01))
#         xspan = np.max(axes[1].get_xticks()) - np.min(axes[1].get_xticks())
        
        # set text just at the middle of the positive area
        text_xpos = 0
        if (min >= 0):
            text_xpos = ((max_ - min_) * .6) + min_
        elif (min < 0): 
            text_xpos = max_ * .5
        text_ypos = np.min(axes[1].get_yticks()) *.1
        axes[1].text(text_xpos, 
                     text_ypos,
                     "max = %0.4f\nmin = %0.4f\nmu = %0.4f\nstd = %0.4f\nmed = %0.4f\nmod = %0.4f\nkrt = %0.4f\nskw = %0.4f\n" % (max_, min_, mean, std, median, mode, kurt, skew),
                     fontsize=8)
        
        data.plot.hist(bins=100, ax=axes[2], title='Histogram')
        axes[2].set_xlim(min_, max_)
     
    #     # Dickey-Fuller test test for stationarity
        sm.graphics.tsa.plot_acf(data.squeeze(), lags=200, ax=axes[3])
         
        sm.graphics.tsa.plot_pacf(data.squeeze(), lags=200, ax=axes[4])
        
        autocorrelation_plot(data, ax=axes[5])
        '''
        End of plots
        '''
        
        '''
        2. Display plots
        '''
        # format date axis
        fig.autofmt_xdate()
        # set font size
        plt.setp([a.get_xticklabels() for a in fig.axes], visible=True, fontsize=PLTFONTSIZE)
        plt.setp([a.get_yticklabels() for a in fig.axes], visible=True, fontsize=PLTFONTSIZE)
        
        # Choose layout spacing
        plt.tight_layout(pad=3, w_pad=0.0, h_pad=1.0)
#         plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=None, hspace=None)
        # Set top title
        plt.suptitle(self.name_, y=1.0)
        # save to pdf
        from matplotlib.backends.backend_pdf import PdfPages
        pp = PdfPages(self.name_+'_'+title+'.pdf')
        plt.savefig(pp, format='pdf')
        pp.close()
        #show to desktop, call is blocking
        plt.show()