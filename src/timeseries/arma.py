from __future__ import division
# import os  
import numpy as np  
import pandas as pd  
# import matplotlib.pyplot as plt
# from matplotlib.backends.backend_pdf import PdfPages  
import statsmodels.api as sm  
# from statsmodels.tsa.stattools import adfuller, acf
import multiprocessing as mp
from scipy import stats
# from datetime import datetime
# import statsmodels
from statsmodels.stats import diagnostic
from pandas.tseries.offsets import BDay # for generating business days
from statsmodels.tsa import stattools # manual acf and pacf
from plot import plot_autocorrelation, plot_stationarity, plot_ts


TRADINGDAYS = 252

def is_stationary(ts, name, window=40, crit=0.05):
    assert (crit is '1%' or crit is '5%' or crit is '10%')
    # http://stackoverflow.com/questions/279561/what-is-the-python-equivalent-of-static-variables-inside-a-function
    is_stationary.crits_ = {0.01:'1%', 0.05:'5%', 0.1:'10%'}
    ret = False
    '''
    [1] Unit-Root Test
    '''
    # Augmented Dickey-Fuller Test
    # Test statistic must be smaller thatn the crititical value
    # H_0 == Has Unit-Root (not stationary)
    # H_1 == Has no Unit-Root (stationary)
    # if test-statistic is smalller, we reject the null hypthesis
    # Alternatively, if p-value is less than 0.05 reject the null hypothesis
    # http://statsmodels.sourceforge.net/devel/generated/statsmodels.tsa.stattools.adfuller.html
    dftest = stattools.adfuller(ts.values, autolag='AIC')
    #@{print result to console
    critical_values = 'critical values:\n'
    for key,value in dftest[4].items():
        critical_values += '(%s)\t\t%0.8f\n' % (key,value)
    print ('\nADF Test:\t%s\n'
           't-statistic\t%.8f\n'
           'p-value\t\t%.8f\n'
           'lags used\t%d\n'
           'noobs\t\t%d\n'
           '%s\n' % (name, dftest[0], dftest[1], dftest[2], dftest[3], critical_values))
    #@}
    pvalue = dftest[1]
    tstatistic = dftest[0]
    critical_value = dftest[4][is_stationary.crits_[crit]]
    if (tstatistic > critical_value):
        if (pvalue > crit):
            ret = True
    return ret
            
def is_autocorrelated(x, name, window=40):
    lb_pass = False
    dw_pass = False
    '''
    [1] LJung-Box Test
    ''' 
    # https://www.r-bloggers.com/story-of-the-ljung-box-blues-progress-not-perfection/
    # http://www.itl.nist.gov/div898/handbook/pmc/section4/pmc4481.htm
    # Ljung-box requires input to be stationary and has a mean of 0.
    # Test normality
    # H0: The data are independently distributed (i.e. the correlations in the population 
    #     from which the sample is taken are 0, so that any observed correlations in the 
    #     data result from randomness of the sampling process).
    # Ha: The data are not independently distributed; they exhibit serial correlation.   
    lb = diagnostic.acorr_ljungbox(x, lags=window, boxpierce=False)
    # p-values must be greater than critical values eg 0.05
    pvals = lb[1]
    rejection_region = pvals[pvals < 0.05]
    if len(rejection_region) <= 0:
        lb_pass = True
        print ("%s is independently distributed, not autocorrelated" % (name))
    else:
        print ("%s is NOT independently distributed" % (name))
        
    '''
    [2] Durbin-Watson
    The Durbin Watson statistic is a number that tests for autocorrelation in the residuals 
    from a statistical regression analysis. The Durbin-Watson statistic is always between 0 and 4.
    A value of 2 means that there is no autocorrelation in the sample. Values approaching 0 
    indicate positive autocorrelation and values toward 4 indicate negative autocorrelation.
    Note: Durbin-watson result is compromised if the model uses AR component
    http://stats.stackexchange.com/questions/154167/why-ever-use-durbin-watson-instead-of-testing-autocorrelation 
    '''
    durbin_watson = sm.stats.durbin_watson(x)
    durbin_watson_rounded = np.math.ceil(durbin_watson*(10**1)/(10**1))
    print ("%s durbin-watson test is %0.8f (rounded)=%0.8f" % (name, durbin_watson, durbin_watson_rounded))
    
    if (durbin_watson_rounded == 2.0):
        dw_pass = True
    else:
        print ("%s failed Durbin-Watson test" % (name))
    
    return lb_pass and dw_pass


def get_pq(x, lags=40, alpha=0.05):
    '''
    https://people.duke.edu/~rnau/411arim3.htm
    Detrermines the ARMA parameters where p is determined from pacf and used
    as parameter for AR component and q from the acf test for the MA component
    Note:
    [1] If you have correctly identified the model,the highest-order AR or MA coefficient should 
    be significantly different from zero according to the usual standard for a regression model, 
    i.e., it should have a t-stat greater than 2 in magnitude and correspondingly a P-value less 
    than 0.05. In applying this test, you only look at the highest order coefficient, not the lower-
    order ones.
    
    [2] ACF that dies out gradually and PACF that cuts off  sharply after a few lags, 
        usually positively correlated at lag 1 (or non-stationary), AR signature
    [3] ACF that cuts off sharply after a few lags and PACF that dies out more gradually 
        usually negatively correlated at lag 1 (or over-differenced), AR signature
    [4] If ACF is gradually decreasing, add differencing
    '''
    assert (len(x) >= lags)
    # internal static function
    def find_significant_lags(lags, qstat, pvals):
        lags_ = [] # significant lags
        for i in xrange(0, len(lags)):
            lag = p[i]
            print ("get_pq: [%d] qstat=%0.8f pval=%0.8f" % (lag, qstat[lag], pvals[lag]))
            if (qstat[lag] >= 2.0 and pvals[lag] > alpha):
                lags_.append(lags[i])
        return lags_
    
    def find_lags(x, interval):
        lags = []
        for i in xrange(1, len(x)-1):
            val = x[i]
            if val > 0 and val > interval:
                lags.append(i)
            if (val < 0 and val < -interval):
                lags.append(i)
        return lags
                
    #z=1.96
    z = stats.norm.ppf(1-alpha)
    print 'get_pq:z=', z
    interval = z / np.sqrt(len(x))
    print 'get_pq:interval=', interval
    
    
    acf, confInt, qstat, pvals  = stattools.acf(x, nlags=lags, alpha=alpha, qstat=True)
    p = find_lags(acf, interval)
    p_ = find_significant_lags(p, qstat, pvals)
    if (len(p_) <= 0):
        print ("get_pq: No significant ACF lags")
        
    pacf, _  = stattools.pacf(x, nlags=lags, alpha=alpha)
    q = find_lags(pacf, interval)
    
    return p, q

def predict(x, title, order):
    '''
    [1] Train
    '''
    assert (x.shape[0])
    model = sm.tsa.ARMA(x, order=order)
    result = model.fit(disp=-1)
    y = result.fittedvalues
    resid = result.resid
        
    '''
    [2] Validate model
    '''
    '''
    [3] Test residual for serial correlation
    ''' 
    is_autocorrelated(resid, ('%s_arma_%s_residual' % (title,order)), 40)
    # the lower aic and bic the better
    rss = np.sum((y - x)**2)
    rmse = sm.tools.eval_measures.rmse(x.values, y.values)
    print('\nARMA results for %s\naic:\t\t%0.8f\nbic:\t\t%0.8f\nrss:\t\t%0.8f\nrmse:\t\t%0.8f\n' % 
          (title, result.aic, result.bic, rss, rmse))
    
    #{@print results
    df = pd.DataFrame(data={'x':x.values, 'y':y.values}, index=x.index)
    mp.Process(target=plot_ts, args=(df, 'arma_' + title)).start()
    #print residual
    resid_ = pd.DataFrame(data=resid)
    mp.Process(target=plot_ts, args=(resid_, "arma_residual_"+title)).start()
    mp.Process(target=plot_stationarity, args=(resid, 'arma_resid_'+title, TRADINGDAYS)).start()
    mp.Process(target=plot_autocorrelation, args=(resid_, 'arma_resid_'+title)).start()
    #}@print results
    return result
    
    
def forecast(x, result, steps=1, useiteration=True):
    '''
    @param x
    @type x: pandas.Series
    @param result
    @type result: ARMAResult
    @param steps
    @return Series
    https://groups.google.com/forum/#!topic/pystatsmodels/_ItLBVpePIY
    http://machinelearningmastery.com/time-series-forecast-uncertainty-using-confidence-intervals-python/
    '''
    if (useiteration):
        order = (result.k_ar, result.k_ma)
        print "order ", order
    
        for i in xrange(0, steps):
            pred, sterr, confint= result.forecast(steps=1)
            
            next_date = (x.index[-1] + BDay(1)).to_pydatetime()
            x = x.append(pd.Series(pred[0], index=[next_date]))
    
            model = sm.tsa.ARMA(x[i:], order=order)
            result = model.fit(disp = -1)
            y = result.fittedvalues
            rmse = sm.tools.eval_measures.rmse(x.values[i:], y.values)
            print ("iteration %d rmse=%0.8f" % (i, rmse))
            # Check residual
            resid = result.resid 
            lb = diagnostic.acorr_ljungbox(resid, lags=40, boxpierce=False)
            # p-values must be greater than critical values eg 0.05
            pvals = lb[1]
            rejection_region = pvals[pvals < 0.05]
            if (len(rejection_region) > 0):
                print ("iteration %d failed ljung-box test")
                break
        
        return x.tail(steps)
    else:
        pred, sterr, confint= result.forecast(steps=steps)
        forecast = pd.Series()
        # stich pred to date steps
        current_date = x.index[-1]
        for i in xrange(0, steps):
            next_date = (current_date + BDay(1)).to_pydatetime()
            forecast.append(pd.Series(pred[0], index=[next_date]))
            current_date = next_date
        return forecast
    
def diffinv(diff, initial):
    ret = pd.Series(0.0, index=diff.index)
    prev = initial
    for i in xrange(0, len(diff)):
        ret[i] = prev = prev + diff[i]
    return ret
