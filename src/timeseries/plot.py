
from __future__ import division
# import os  
import numpy as np  
import pandas as pd  
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages  
import statsmodels.api as sm  
# from statsmodels.tsa.stattools import adfuller, acf
# import multiprocessing as mp
# from scipy import stats
# from datetime import datetime
# import statsmodels
# from statsmodels.stats import diagnostic
# from pandas.tseries.offsets import BDay # for generating business days
from statsmodels.tsa import stattools # manual acf and pacf

RESULTPATH = '/home/akalingking/src/python/v2/oanda_analytics/results/'
PLTFONTSIZE = 8
RCPARAMS = {'axes.titlesize':PLTFONTSIZE,
             'axes.labelsize':PLTFONTSIZE,
             'legend.fontsize':PLTFONTSIZE,
             'xtick.labelsize':PLTFONTSIZE,
             'ytick.labelsize':PLTFONTSIZE,}

def plot_ts(data, title=None, resultpath=RESULTPATH, show=False):
    '''
    @param data datafram instance
    '''
    fig, axes = plt.subplots(nrows=1, ncols=1, sharex=False, sharey=False, figsize=(12,10))
    plt.rcParams.update(RCPARAMS)
    data.plot.line(ax=axes)
    
    fig.autofmt_xdate()
    # set font size
    plt.setp([a.get_xticklabels() for a in fig.axes], visible=True, fontsize=5)
    plt.setp([a.get_yticklabels() for a in fig.axes], visible=True, fontsize=5)
     
    # Choose layout spacing
    plt.tight_layout(pad=2, w_pad=0.0, h_pad=1.0)
#         plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=None, hspace=None)
    # Set top title
    plt.suptitle(title, y=1.0)
    
    # save to pdf
    pp = PdfPages(resultpath + title + '.pdf')
    plt.savefig(pp, format='pdf')
    pp.close()
    
    # show to desktop, call is blocking
    if show:
        plt.show()

def plot_stationarity(data, title, window, resultpath=RESULTPATH, show=False):
    '''
    @param data    series
    @param title   name of series
    @param window  lag  
    '''
    '''
    [1] Variables for stationarity plots
    '''
    rollingMean = pd.Series.rolling(data, window=window).mean()
    rollingStd = pd.Series.rolling(data, window=window).std()
    s = pd.DataFrame(data=data, index=data.index, columns=['Original'])
    s['Rolling-Mean'] = rollingMean
    s['Rolling-Stdev'] = rollingStd
    '''
    [2] Decompose timeseries
    '''
    composition = sm.tsa.seasonal_decompose(data, model='additive', freq=window)
    '''
    [3] Show plots
    '''    
    fig, axes = plt.subplots(nrows=4, ncols=1, sharex=False, sharey=False, figsize=(10,15))
    plt.rcParams.update(RCPARAMS)
    
    #{@ Plot
    s.plot(ax=axes[0], title='Data', legend='best')
    composition.trend.plot(ax=axes[1], title='Trend')
    composition.seasonal.plot(ax=axes[2], title='Seasonal')
    composition.resid.plot(ax=axes[3], title='Residual')
    #}@plot

    fig.autofmt_xdate()
    # set font size
    plt.setp([a.get_xticklabels() for a in fig.axes], visible=True, fontsize=PLTFONTSIZE)
    plt.setp([a.get_yticklabels() for a in fig.axes], visible=True, fontsize=PLTFONTSIZE)
    
    # Choose layout spacing
    plt.tight_layout(pad=3, w_pad=0.0, h_pad=2.0)
#         plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=None, hspace=None)
    # Set top title
    plt.suptitle(title, y=1.0)
    
    # save to pdf
    pp = PdfPages(resultpath +'stationarity_' + title + '.pdf')
    plt.savefig(pp, format='pdf')
    pp.close()
    
    #show to desktop, call is blocking
    if show:
        plt.show()


def plot_autocorrelation(data, title, window=40, ymax=0.05, resultpath=RESULTPATH, show=False):
    '''
    Plots autocorrelation
    '''
    # Prepare figure    
    fig, axes = plt.subplots(nrows=4, ncols=1, sharex=False, sharey=False, figsize=(10,15))
    plt.rcParams.update(RCPARAMS)
    # plot the original data
    data.plot.line(ax=axes[0], title=title)
    
    #{ plot
    acf_ = stattools.acf(data, nlags=window) # actual data, just to get range of diplay
    sm.graphics.tsa.plot_acf(data.values.squeeze(), lags=window, ax=axes[1])
    ymax = np.sort(acf_)[-2] *1.5
    axes[1].set_ylim(-ymax, ymax)
    
    pacf_ = stattools.pacf(data, nlags=window) # actual data, just to get range of diplay
    sm.graphics.tsa.plot_pacf(data, lags=window, ax=axes[2])
    ymax = np.sort(pacf_)[-2] *1.5 
    axes[2].set_ylim(-ymax, ymax)
    
    # http://statsmodels.sourceforge.net/devel/generated/statsmodels.graphics.gofplots.qqplot.html
    sm.qqplot(data=data, ax=axes[3], line='r')
    #} plot
    
    fig.autofmt_xdate()
    # set font size
    plt.setp([a.get_xticklabels() for a in fig.axes], visible=True, fontsize=PLTFONTSIZE)
    plt.setp([a.get_yticklabels() for a in fig.axes], visible=True, fontsize=PLTFONTSIZE)
    # Choose layout spacing
    plt.tight_layout(pad=3, w_pad=0.0, h_pad=2.0)
#         plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=None, hspace=None)
    # Set top title
    plt.suptitle('Correlation: ' + title, y=1.0)
    
    # save to pdf
    pp = PdfPages(resultpath + 'correlation_' + title + '.pdf')
    plt.savefig(pp, format='pdf')
    pp.close()
    
    #show to desktop, call is blocking
    if show:
        plt.show()