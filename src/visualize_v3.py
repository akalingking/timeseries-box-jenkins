import ohlcdescstat
import sys

def main():
    if (len(sys.argv) != 2):
        print("usage: %s <filename>" % sys.argv[0])
        sys.exit()
    
    ohlcdescstat.OhlcDescStat(sys.argv[1])
    
    raw_input('\npress any to exit..\n')
    exit()
    

if __name__=='__main__':
    main()
    